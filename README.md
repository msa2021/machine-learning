# machine-learning

Example springboot project to call Watson machine learning service

## Getting started

1. Create Watson machine learning service from https://cloud.ibm.com
2. Create IAM API Key from https://cloud.ibm.com
3. Import project from STS4(https://spring.io/tools)
4. Edit IAM API KEY and WML URL in the \machine-learning_temp\product_recommend\ProductController.java file
5. Paste the code to "com\springboot\microservices\sample\rest\ProductController.java" file in the corresponding method.
6. Run the springboot application
