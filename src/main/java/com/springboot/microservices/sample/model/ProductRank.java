package com.springboot.microservices.sample.model;

import lombok.Data;

@Data
public class ProductRank {
    private String  clusterItem1 		; // 많이 구매한 제품 랭킹 1
    private String  clusterItem2 		; // 많이 구매한 제품 랭킹 2
    private String  clusterItem3 	    ; // 많이 구매한 제품 랭킹 3
    private String  clusterItem4 	    ; // 많이 구매한 제품 랭킹 4
    private String  clusterItem5        ; // 많이 구매한 제품 랭킹 5
}
