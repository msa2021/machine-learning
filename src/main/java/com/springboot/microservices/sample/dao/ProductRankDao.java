package com.springboot.microservices.sample.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.springboot.microservices.sample.model.ProductRank;

@Mapper
public interface ProductRankDao {

    /**
     * 제품 랭킹 정보 가져오기
     * @return
     * @throws Exception
     */
    List<ProductRank> selectProductRank(String id) throws Exception;
}
