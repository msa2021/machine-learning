package com.springboot.microservices.sample.rest;

import com.springboot.microservices.sample.dao.ProductRankDao;
import com.springboot.microservices.sample.model.ProductRank;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.MalformedURLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

@Slf4j
@Api(value="Hello Service API")
@RestController
public class ProductController {

    @Autowired
    private ProductRankDao productRankDao;

    @ApiOperation(value="클러스터 별 제품 랭킹 DB 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name="cluster", value="클러스터 ID: 0~9",required = true, dataType="String", paramType="query")
    })
    @RequestMapping(value="/cluster/product", method= RequestMethod.GET)
    public String getClusterRank(@RequestParam(value="cluster" ,required = true) String cluster ) {
        return getClusterRankMethod(cluster);
    }

    public String getClusterRankMethod(String cluster){
        String rank = "";
        List<ProductRank> prr = null;
        try {
            log.info("Start db select");
            prr = productRankDao.selectProductRank(cluster);
            log.debug("user counts :"+prr.size());
            rank = prr.get(0).getClusterItem1()+ ", " +
                    prr.get(0).getClusterItem2() + ", " +
                    prr.get(0).getClusterItem3() + ", " +
                    prr.get(0).getClusterItem4() + ", " +
                    prr.get(0).getClusterItem5() + ", ";
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("rank result :" + rank);
        return rank;
    }

    @ApiOperation(value="사용자 클러스터 판별 REST API 서비스 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name="state_numb", value="도번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="city_numb", value="시번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="age_group", value="연령그룹: 0~9",required = true, dataType="String", paramType="query"),
    })
    @RequestMapping(value="/cluster/predict", method= RequestMethod.GET)
    public String predictCluster(@RequestParam(value="state_numb", required = true) String state_numb,
                                 @RequestParam(value="city_numb", required = true) String city_numb,
                                 @RequestParam(value="age_group", required = true) String age_group) {
        return predictClusterMethod(state_numb, city_numb, age_group);
    }

    public String predictClusterMethod(String state_numb, String city_numb, String age_group){
    	//TODO: product_recommend/ProductController.java 내용을 참고하여 작성을 완료 
    	String result = "";
        
        return result;
    }

    @ApiOperation(value="클러스터 판별에 따른 인기 상품 조회(WML Rest API + DB 조회)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="state_numb", value="도번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="city_numb", value="시번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="age_group", value="연령그룹: 0~9",required = true, dataType="String", paramType="query"),
    })
    @RequestMapping(value="/product/predict", method= RequestMethod.GET)
    public String predictProduct(@RequestParam(value="state_numb", required = true) String state_numb,
                                 @RequestParam(value="city_numb", required = true) String city_numb,
                                 @RequestParam(value="age_group", required = true) String age_group) {
    	//TODO: product_recommend/ProductController.java 내용을 참고하여 작성을 완료
        String tempResult = "";
        
        return tempResult;
    }
}
