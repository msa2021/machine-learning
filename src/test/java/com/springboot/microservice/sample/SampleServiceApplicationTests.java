package com.springboot.microservice.sample;

import com.springboot.microservices.sample.SampleServiceApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SampleServiceApplication.class)
class SampleServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
