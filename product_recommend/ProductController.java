
    @ApiOperation(value="사용자 클러스터 판별 REST API 서비스 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name="state_numb", value="도번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="city_numb", value="시번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="age_group", value="연령그룹: 0~9",required = true, dataType="String", paramType="query"),
    })
    @RequestMapping(value="/cluster/predict", method= RequestMethod.GET)
    public String predictCluster(@RequestParam(value="state_numb", required = true) String state_numb,
                                 @RequestParam(value="city_numb", required = true) String city_numb,
                                 @RequestParam(value="age_group", required = true) String age_group) {
        return predictClusterMethod(state_numb, city_numb, age_group);
    }

    public String predictClusterMethod(String state_numb, String city_numb, String age_group){
        String result = "";
        // NOTE: you must manually set API_KEY below using information retrieved from your IBM Cloud account.

        String API_KEY = "o1ttqzp6wy0zSvzvxTBeOLrAiUL0V1aFHCYqT8NznIs0";

        HttpURLConnection tokenConnection = null;
        HttpURLConnection scoringConnection = null;
        BufferedReader tokenBuffer = null;
        BufferedReader scoringBuffer = null;
        try {
            // Getting IAM token
            URL tokenUrl = new URL("https://iam.cloud.ibm.com/identity/token?grant_type=urn:ibm:params:oauth:grant-type:apikey&apikey=" + API_KEY);
            tokenConnection = (HttpURLConnection) tokenUrl.openConnection();
            tokenConnection.setDoInput(true);
            tokenConnection.setDoOutput(true);
            tokenConnection.setRequestMethod("POST");
            tokenConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            tokenConnection.setRequestProperty("Accept", "application/json");
            tokenBuffer = new BufferedReader(new InputStreamReader(tokenConnection.getInputStream()));
            StringBuffer jsonString = new StringBuffer();
            String line;
            while ((line = tokenBuffer.readLine()) != null) {
                jsonString.append(line);
            }
            // Scoring request
            URL scoringUrl = new URL("https://jp-tok.ml.cloud.ibm.com/ml/v4/deployments/2a13fe89-34dd-4fd0-b9dd-5d98d49f3f0e/predictions?version=2021-09-28&version=2021-09-28");
            String iam_token = "Bearer " + jsonString.toString().split(":")[1].split("\"")[1];
            scoringConnection = (HttpURLConnection) scoringUrl.openConnection();
            scoringConnection.setDoInput(true);
            scoringConnection.setDoOutput(true);
            scoringConnection.setRequestMethod("POST");
            scoringConnection.setRequestProperty("Accept", "application/json");
            scoringConnection.setRequestProperty("Authorization", iam_token);
            scoringConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            OutputStreamWriter writer = new OutputStreamWriter(scoringConnection.getOutputStream(), "UTF-8");

            // NOTE: manually define and pass the array(s) of values to be scored in the next line
            String payload = "{\"input_data\": [{\"fields\": [\"CITY\", \"STATE\", \"GENERATION\", \"Baby Food\", \"Diapers\", \"Formula\", \"Lotion\", \"Baby wash\", \"Wipes\", \"Fresh Fruits\", \"Fresh Vegetables\", \"Beer\", \"Wine\", \"Club Soda\", \"Sports Drink\", \"Chips\", \"Popcorn\", \"Oatmeal\", \"Medicines\", \"Canned Foods\", \"Cigarettes\", \"Cheese\", \"Cleaning Products\", \"Condiments\", \"Frozen Foods\", \"Kitchen Items\", \"Meat\", \"Office Supplies\", \"Personal Care\", \"Pet Supplies\", \"Sea Food\", \"Spices\"],"
                    + "\"values\": [[" + city_numb + ", " + state_numb + ", " + age_group + ", 1, 2, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]]}]}";

            writer.write(payload);
            writer.close();

            scoringBuffer = new BufferedReader(new InputStreamReader(scoringConnection.getInputStream()));
            StringBuffer jsonStringScoring = new StringBuffer();
            String lineScoring;
            while ((lineScoring = scoringBuffer.readLine()) != null) {
                jsonStringScoring.append(lineScoring);
            }
            result = result+jsonStringScoring;
            System.out.println(jsonStringScoring);
        } catch (IOException e) {
            System.out.println("The URL is not valid.");
            System.out.println(e.getMessage());
        }
        finally {
            if (tokenConnection != null) {
                tokenConnection.disconnect();
            }
            if (tokenBuffer != null) {
                try {
                    tokenBuffer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (scoringConnection != null) {
                scoringConnection.disconnect();
            }
            if (scoringBuffer != null) {
                try {
                    scoringBuffer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @ApiOperation(value="클러스터 판별에 따른 인기 상품 조회(WML Rest API + DB 조회)")
    @ApiImplicitParams({
            @ApiImplicitParam(name="state_numb", value="도번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="city_numb", value="시번호: 0~9",required = true, dataType="String", paramType="query"),
            @ApiImplicitParam(name="age_group", value="연령그룹: 0~9",required = true, dataType="String", paramType="query"),
    })
    @RequestMapping(value="/product/predict", method= RequestMethod.GET)
    public String predictProduct(@RequestParam(value="state_numb", required = true) String state_numb,
                                 @RequestParam(value="city_numb", required = true) String city_numb,
                                 @RequestParam(value="age_group", required = true) String age_group) {
        String result =  predictClusterMethod(state_numb, city_numb, age_group);
        String tempResult = "";
        try {
            JSONObject jObject = new JSONObject(result);
            Integer tempJO = (Integer)jObject.getJSONArray("predictions").getJSONObject(0).getJSONArray("values").getJSONArray(0).get(0);
            log.info("WML result: " + tempJO.toString());
            tempResult = getClusterRankMethod(tempJO.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tempResult;
    }

