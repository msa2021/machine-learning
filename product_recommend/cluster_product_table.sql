USE demo;

DROP TABLE IF EXISTS tb_product_cluster_user01;

CREATE TABLE tb_product_cluster_user01  (
    cluster_id varchar(20) NOT NULL,
    cluster_item_1 varchar(60) NOT NULL,
    cluster_item_2 varchar(60) NOT NULL,
    cluster_item_3 varchar(60) NOT NULL,
    cluster_item_4 varchar(60) NOT NULL,
    cluster_item_5 varchar(60) NOT NULL,
    primary key (cluster_id)
);

insert into tb_product_cluster_user01 (cluster_id, cluster_item_1, cluster_item_2, cluster_item_3, cluster_item_4, cluster_item_5)
values ('0', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('1', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('2', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('3', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('4', 'Club Soda', 'Canned Foods', 'Chips', 'Condiments', 'Popcorn'),
('5', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('6', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments'),
('7', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Wine'),
('8', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Wine'),
('9', 'Club Soda', 'Canned Foods', 'Chips', 'Popcorn', 'Condiments');
